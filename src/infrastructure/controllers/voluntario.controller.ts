import { Body, Controller, Get, HttpStatus, NotFoundException, Param, Post, Req, Res } from '@nestjs/common';
import { CreateVoluntarioDTO } from '../../domain/dto/voluntario.dto';
import { VoluntarioService } from '../services/voluntario.service';
import { VoluntarioRegisterCase } from '../../application/case-resgisterVoluntary';

@Controller('voluntario')
export class VoluntarioController {
    constructor(
        private voluntarioServiceCase: VoluntarioRegisterCase,
        private voluntarioService: VoluntarioService
    ){}
    
    @Post('/create')
    async register(@Res() res, @Body() createVoluntarioDTO: CreateVoluntarioDTO): Promise<any> {

        let service = await this.voluntarioService.create(createVoluntarioDTO);
        // let service = await this.voluntarioServiceCase.create(createVoluntarioDTO);
    
        if(!service) throw new NotFoundException('register fail');

        return res.status(HttpStatus.OK).json(service);

    }

    @Get('/')
    async getVoluntario(@Res() res, @Req() req): Promise<any> {

        let service = await this.voluntarioService.all();

        if(!service) throw new NotFoundException('data null');

        return res.status(HttpStatus.OK).json(service);

    }

    @Get('/:rut')
    async getVoluntarioId(@Res() res, @Req() req, @Param('rut') rut): Promise<any> {

        let service = await this.voluntarioService.One(rut);

        if(!service) throw new NotFoundException('data null');

        return res.status(HttpStatus.OK).json(service);

    }
}
