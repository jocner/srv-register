import { Body, Controller, Get, HttpStatus, NotFoundException, Post, Res } from '@nestjs/common';
import { UserService } from '../services/user.service';
import { UserDTO } from '../../domain/dto/user.dto';

@Controller('user')
export class UserController {
    constructor(
        private userService: UserService
    ){}

    @Post('/create')
    async register(@Res() res, @Body() userDTO: UserDTO): Promise<any> {

        let service = await this.userService.create(userDTO);
    
        if(!service) throw new NotFoundException('register fail');

        return res.status(HttpStatus.OK).json(service);

    }

    @Get('/')
    async find(@Res() res, @Body() userDTO:UserDTO):Promise<any> {

        let service = await this.userService.all();

        if(!service) throw new NotFoundException('data null');

        return res.status(HttpStatus.OK).json(service);

    }


}
