import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Voluntario } from '../../domain/entities/voluntario.entity';
import { CreateVoluntarioDTO } from '../../domain/dto/voluntario.dto';
const nodemailer = require("nodemailer");

@Injectable()
export class MailerService {
    constructor(
        @InjectRepository(Voluntario) private voluntarioRepo: Repository<Voluntario>
    ){}

    async mailer(createVoluntarioDTO: CreateVoluntarioDTO): Promise<any>{

        // const voluntario = this.voluntarioRepo.find();
        // const volun = this.voluntarioRepo.findOne();

        console.log("desde mailer", createVoluntarioDTO.correo);
        console.log("enviroment", process.env.EMAIL);

        let jConfig = await nodemailer.createTransport({
            service: 'gmail',
            auth: {
              user: 'jocner.p.b@gmail.com',
              pass: 'David.4857'
            }
          });

          let mailOptions = {
            from: 'jocner.p.b@gmail.com',
            to: 'jocner.p.b@gmail.com',
            subject: 'Asunto Del Correo',
            html:` 
           <div> 
           <p>Hola amigo</p> 
           <p>Esto es una prueba del vídeo</p> 
           <p>¿Cómo enviar correos eletrónicos con Nodemailer en NodeJS </p> 
           </div> 
       ` 
          };

        let envio = await jConfig.sendMail(mailOptions, function(error, info){
            if (error) {
              console.log(error);
            } else {
              console.log('Email enviado: ' + info.response);
            }
          });

        return envio;
    }
    
}
