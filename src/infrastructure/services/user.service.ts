import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../../domain/entities/user.entity';
import { UserDTO } from '../../domain/dto/user.dto';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(User) private userRepo: Repository<User>
    ){}

    async create(userDTO: UserDTO):Promise<any> {

        const password = userDTO.password;

        const round = 10;
        const passEncrip = await bcrypt.hash(password, round);
        
      
        console.log("passenc", passEncrip)
        const newUser = await this.userRepo.create({ nombres: userDTO.nombres,
        apellido_paterno : userDTO.apellido_paterno, apellido_materno: userDTO.apellido_materno,
        correo: userDTO.correo, password: userDTO.password, campus: userDTO.campus,
        });
        
        const register = await this.userRepo.save(newUser);

        return register;

    }

    async all():Promise<any> {

        const user = await this.userRepo.find();

        console.log("sdasdas", user)

        return user;

    }
}
