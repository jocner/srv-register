import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Voluntario } from '../../domain/entities/voluntario.entity';
import { CreateVoluntarioDTO } from '../../domain/dto/voluntario.dto';
// import { bcrypt } from 'bcryptjs';
import * as bcrypt from 'bcrypt';
// const bcrypt = require('bcrypt');

@Injectable()
export class VoluntarioService {
    constructor(
        @InjectRepository(Voluntario) private voluntarioRepo: Repository<Voluntario>
    ){}

    async create(createVoluntarioDTO: CreateVoluntarioDTO): Promise<any>{

        const password = createVoluntarioDTO.password;

        // bcrypt

        const round = 10;
        const passEncrip = await bcrypt.hash(password, round);
        
        // console.log("wqeq", createVoluntarioDTO)
        console.log("passenc", passEncrip)
        const newVoluntario = await this.voluntarioRepo.create({ nombres: createVoluntarioDTO.nombres,
        apellido_paterno : createVoluntarioDTO.apellido_paterno, apellido_materno: createVoluntarioDTO.rut_dns,
        correo: createVoluntarioDTO.correo, password: createVoluntarioDTO.password, campus: createVoluntarioDTO.campus,
        });
        // const newVoluntario = await this.voluntarioRepo.create(createVoluntarioDTO);
        const register = await this.voluntarioRepo.save(newVoluntario);

        return register;

    }

    async all(): Promise<any>{

        const voluntario = this.voluntarioRepo.find();
        const volun = this.voluntarioRepo.findOne();


        return voluntario;
    }

    async One(id): Promise<any>{

        const voluntario = this.voluntarioRepo.findOne(id);

        return voluntario;
    }
}
