import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { VoluntarioRegisterCase } from '../application/case-resgisterVoluntary'
import { MailerController } from './controllers/mailer.controller';
import { MailerService } from './services/mailer.service';
import { VoluntarioController } from './controllers/voluntario.controller';
import { UserController } from './controllers/user.controller';
import { VoluntarioService } from './services/voluntario.service';
import { UserService } from './services/user.service';
import { Voluntario } from '../domain/entities/voluntario.entity';
import { User } from '../domain/entities/user.entity';


@Module({
    imports: [
        TypeOrmModule.forFeature([Voluntario, User])
    ],
    providers: [VoluntarioService, UserService, MailerService, VoluntarioRegisterCase],
    controllers: [VoluntarioController, MailerController, UserController]
})
export class FollowersModule {}
