import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FollowersModule } from './infrastructure/followers.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mongodb',
      url: 'mongodb+srv://jocner:jocner@cluster0.axw6l.mongodb.net/swagger-typescript?retryWrites=true&w=majority',
      useNewUrlParser: true,
      logging: true,
      synchronize: false,
      entities: ['dist/**/**/*.entity{.ts,.js}']
    }),
    FollowersModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}


// TypeOrmModule.forRoot({
//   type: 'mongodb',
//   host: process.env.HOST,
//   port: 27017,
//   username: process.env.USER_DB,
//   password: process.env.PASSWORD_DB,
//   database: process.env.NAME_DB,
//   entities: ['dist/**/**/*.entity{.ts,.js}'],
//   synchronize: false,
// }),
